﻿

namespace ForeignExchange.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Models;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using System.ComponentModel;
    using System.Collections.Generic;
    using Xamarin.Forms;
    using ForeignExchange.Helpers;
    using ForeignExchange.Services;
    using System;
    using System.Threading.Tasks;

    public class MainViewModel : INotifyPropertyChanged
    {

        #region Eventes
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
        #region Services
        ApiService apiService;
        DataService dataService;
        DialogService dialogService;
        #endregion
        #region Attributes
        bool _isRunning;
        bool _isEnabled;
        Rate _sourceRate;
        Rate _targetRate;
        string _status;
        List<Rate> rates;
        ObservableCollection<Rate> _rates;
        string _result;

        #endregion
        #region Properties

        public string Amount { get; set; }

        public ObservableCollection<Rate> Rates
        {
            get
            {
                return _rates;
            }

            set
            {
                if (_rates != value)
                {
                    _rates = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Rates)));
                }

            }
        }

        public Rate SourceRate
        {
            get
            {
                return _sourceRate;
            }

            set
            {
                if (_sourceRate != value)
                {
                    _sourceRate = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SourceRate)));
                }

            }
        }

        public Rate TargetRate
        {
            get
            {
                return _targetRate;
            }

            set
            {
                if (_targetRate != value)
                {
                    _targetRate = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(TargetRate)));
                }

            }
        }

        public bool IsRunning
        {
            get
            {
                return _isRunning;
            }

            set
            {
                if (_isRunning != value)
                {
                    _isRunning = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsRunning)));
                }

            }
        }

        public string Status
        {
            get
            {
                return _status;
            }

            set
            {
                if (_status != value)
                {
                    _status = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Status)));
                }

            }
        }


        public bool IsEnabled
        {
            get
            {
                return _isEnabled;
            }

            set
            {
                if (_isEnabled != value)
                {
                    _isEnabled = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsEnabled)));
                }

            }
        }

        public string Result
        {
            get
            {
                return _result;
            }

            set
            {
                if (_result != value)
                {
                    _result = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Result)));
                }

            }
        }

        #endregion
        #region Constructors

        public MainViewModel()
        {

            apiService = new ApiService();
            dataService = new DataService();
            dialogService = new DialogService();
            LoadRates();
        }



        #endregion
        #region Methods

        async void LoadRates()
        {

            IsRunning = true;
            Result = Lenguages.Loading;


            var connection = await apiService.CheckConnection();

            if (!connection.IsSuccess)
            {
                LoadLocalData();

            }
            else
            {
                await LoadDataFromAPI();
            }
            /*Archivos de Recursoss*/

            if (rates.Count==0)
            {
                IsRunning = false;
                IsEnabled = false;
                Result = "There are not internet connection and not load previously rates. " +
                    "Please try again internet connection.";
                Status = "No rates loaded";
                return;

            }
  

            Rates = new ObservableCollection<Rate>(rates);
            IsRunning = false;
            IsEnabled = true;
            Result = Lenguages.Ready;
       

        }

        private void LoadLocalData()
        {
            rates = dataService.Get<Rate>(false);
            Status = "Rates loaded from Local.";
        }

        async Task LoadDataFromAPI()
        {
            var url = "http://apiexchangerates.azurewebsites.net";//Application.Current.Resources["URLAPI"].ToString();

            var response = await apiService.GetList<Rate>(url, "/api/Rates");


            if (!response.IsSuccess)
            {
                LoadLocalData();
                return;
            }

            //Se guarda la base datos local 
            rates = ((List<Rate>)response.Result);

            dataService.DeleteAll<Rate>();
            dataService.Save(rates);
            Status = "Rates loaded from internet.";
        }


        #endregion
        #region Commands

        public ICommand ConvertCommand
        {
            get
            {
                return new RelayCommand(Convert);
            }

        }


        public ICommand SwitchCommand
        {
            get
            {
                return new RelayCommand(Switch);
            }

        }

        void Switch()
        {
            var aux = SourceRate;
            SourceRate = TargetRate;
            TargetRate = aux;
            Convert();
        }

        async void Convert()
        {
            if (string.IsNullOrEmpty(Amount))
            {
                await dialogService.ShowMessage(Lenguages.Error,Lenguages.AmountValidation);
                return;
            }

            decimal amount = 0;
            if (!decimal.TryParse(Amount, out amount))
            {
                await dialogService.ShowMessage(Lenguages.Error, Lenguages.AmountNumericValidation);
                return;
            }

            if (SourceRate == null)
            {
                await dialogService.ShowMessage(Lenguages.Error,Lenguages.SourceRateValidation);
                return;

            }

            if (TargetRate == null)
            {
                await dialogService.ShowMessage(Lenguages.Error,Lenguages.TargetRateValidation);
                return;

            }


            var amountConverted = amount / (decimal)SourceRate.TaxRate * (decimal)TargetRate.TaxRate;

            Result = string.Format("{0} {1:C2} = {2} {3:C2}", SourceRate.Code, amount, TargetRate.Code, amountConverted);







        }

        #endregion
    }
}
